@BUSINESS_RULES @BR006 
Feature: Each fund will have one or more share classes associated with it. 

Scenario Outline: <Business Rule> - <Description> 
	Given I start API test "<Business Rule>_CREATE" 
	And I fetch the data from "TestData.xlsx" 
	And I update the request payload with unique identifier text 
	And I add header information to request 
	When I submit the POST request 
	Then I save the response 
	And I validate status code in response 
	And I validate content type in response 
	And I validate specified field values in response 
	Examples: 
		| Business Rule | Description                                                              | Scenario      |
		| BR_API_006_A  | Fund submission SUCCESS with one shareclass associated with it           | POSITIVE TEST |
		| BR_API_006_B  | Fund submission SUCCESS with more than one shareclass associated with it | POSITIVE TEST |
		| BR_API_006_C  | Fund submission FAILURE with NO shareclass associated with it.           | NEGATIVE TEST |
