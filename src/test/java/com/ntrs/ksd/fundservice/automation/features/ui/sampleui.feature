@UI
Feature: First UI test case

  Scenario Outline: Sample UI Script
    Given I open application
    When I search with the '<Product>'
    Examples:
      | Product  |
      | Selenium |
      | Protractor |