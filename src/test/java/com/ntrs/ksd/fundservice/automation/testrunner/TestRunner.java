package com.ntrs.ksd.fundservice.automation.testrunner;

import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/com/ntrs/ksd/fundservice/automation/features", glue = {
        "com.ntrs.ksd.fundservice.automation.stepdefinitions"}, plugin = {
        "com.ntrs.ksd.fundservice.automation.utils.ReportListener"}, tags = {"@UI"})
public class TestRunner {
}
