package com.ntrs.ksd.fundservice.automation.utils;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.cucumber.listener.Reporter;

import io.restassured.*;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class BaseTest extends GlobalVariables {
	// do not put any gluecode
	public Response response = null;
	public String responseStr;
	public RequestSpecification restAssuredGiven = RestAssured.given();

	public static Map<String, Object> globalMap = new HashMap<String, Object>();
	public static boolean isSkipped = false;
	public static boolean isNewFeature = false;
	public static String featureName = "";

	protected static String testScriptName = "";
	protected static List<Hashtable<String, String>> testCaseTestData = null;

	public String getFundBaseUrl() {
		String url = new PropertyLoader().getValue("url.fund");
		return url;
	}

	public String getUmbrellaBaseUrl() {
		String url = new PropertyLoader().getValue("url.umbrella");
		return url;
	}

	public void log2Report(String message) {
		String formattedStr = "";
		if (message.length() > 100) {
			for (String s : message.split("(?<=\\G.{100})"))
				formattedStr = formattedStr + s + "<BR>";
			Reporter.addStepLog(formattedStr.trim());
		} else {
			Reporter.addStepLog(message);
		}
	}

}
