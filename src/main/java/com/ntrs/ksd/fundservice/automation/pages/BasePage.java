package com.ntrs.ksd.fundservice.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class BasePage {

    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public void waitFor(long seconds) {
        try {
            Thread.sleep(TimeUnit.SECONDS.toMillis(seconds));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException("Exception during wait occurred.", e);
        }
    }

    public void clearAndEnter(WebElement webElement, String value) {
        webElement.clear();
        webElement.sendKeys(value);
    }
}
