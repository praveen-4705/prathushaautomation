package com.ntrs.ksd.fundservice.automation.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.GsonBuilder;
import gherkin.deps.com.google.gson.JsonObject;

public class JsonUtils {
	public static void updateJSONFieldAndSave(File jsonFile, HashMap<String, String> fieldValueMap) throws IOException {
		String jsonStr = new String(Files.readAllBytes(Paths.get(jsonFile.toURI())));
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonObject jsonObject = gson.fromJson(jsonStr, JsonObject.class);
		for (String key : fieldValueMap.keySet()) {
			jsonObject.addProperty(key, fieldValueMap.get(key));
		}

		Files.write(jsonFile.toPath(), gson.toJson(jsonObject).getBytes());
	}

	public Map<String, Object> jsonToMap(org.json.JSONObject json) {
		Map<String, Object> retMap = new HashMap<String, Object>();

		if (json != null) {
			retMap = toMap(json);
		}
		return retMap;
	}

	public static Map<String, Object> toMap(org.json.JSONObject jsonObject) {
		Map<String, Object> map = new HashMap<String, Object>();

		Iterator<String> keysItr = jsonObject.keySet().iterator();
		while (keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = jsonObject.get(key);

			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if (value instanceof JSONObject) {
				value = toMap((org.json.JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	public static List<Object> toList(JSONArray array) {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.size(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if (value instanceof JSONObject) {
				value = toMap((org.json.JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}
}
