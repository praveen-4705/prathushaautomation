package com.ntrs.ksd.fundservice.automation.utils;

public class Config {
	PropertyLoader propLoader = new PropertyLoader();

	public static String getFundBaseUrl() {
		return PropertyLoader.getInstance().getValue("url.Fund");
	}

	public static String getUmbrellaUrl() {
		return PropertyLoader.getInstance().getValue("url.umbrella");
	}
}
