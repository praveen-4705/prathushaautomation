package com.ntrs.ksd.fundservice.automation.stepdefinitions.ui;

import com.ntrs.ksd.fundservice.automation.utils.Driver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class UIStepDefinitions extends Driver {

  @Given("^I open application$")
  public void iOpenApplication() {
    getDriver().get("http://www.google.com");
  }

  @When("^I search with the '(.+)'$")
  public void iSearchWithGivenKey(String value) {
    homePage.searchWith(value);
  }
}