package com.ntrs.ksd.fundservice.automation.utils;

import com.ntrs.ksd.fundservice.automation.pages.HomePage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class Driver {

    protected static ThreadLocal<WebDriver> driver = new ThreadLocal<>();
    protected HomePage homePage;

    public Driver() {
        createPageObjects();
    }

    private void chromeBrowserSetup() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--disable-extensions");
        chromeOptions.addArguments("--start-maximized");
        ChromeDriverManager.getInstance().setup();
        driver.set(new ChromeDriver(chromeOptions));
    }

    protected void startBrowser() {
        chromeBrowserSetup();
        driver.get().manage().window().maximize();
        driver.get().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    protected void closeBrowser() {
        driver.get().quit();
    }

    protected WebDriver getDriver() {
        return driver.get();
    }

    public static void setDriver(ThreadLocal<WebDriver> driver) {
        Driver.driver = driver;
    }

    private void createPageObjects() {
        homePage = new HomePage(getDriver());
    }
}
