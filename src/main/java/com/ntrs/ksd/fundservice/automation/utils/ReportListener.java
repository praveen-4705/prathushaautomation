package com.ntrs.ksd.fundservice.automation.utils;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import gherkin.formatter.Formatter;
import gherkin.formatter.Reporter;
import gherkin.formatter.model.Background;
import gherkin.formatter.model.Examples;
import gherkin.formatter.model.Feature;
import gherkin.formatter.model.Match;
import gherkin.formatter.model.Result;
import gherkin.formatter.model.Scenario;
import gherkin.formatter.model.ScenarioOutline;
import gherkin.formatter.model.Step;
import gherkin.formatter.model.Tag;

public class ReportListener implements Reporter, Formatter {

  private static ThreadLocal<ExtentReports> extentReports = new InheritableThreadLocal<>();
  private static ThreadLocal<ExtentHtmlReporter> htmlReporter = new InheritableThreadLocal<>();
  private static ThreadLocal<ExtentTest> featureTestThreadLocal = new InheritableThreadLocal<>();
  private static ThreadLocal<ExtentTest> scenarioOutlineThreadLocal = new InheritableThreadLocal<>();
  static ThreadLocal<ExtentTest> scenarioThreadLocal = new InheritableThreadLocal<>();
  private static ThreadLocal<LinkedList<Step>> stepListThreadLocal = new InheritableThreadLocal<>();
  static ThreadLocal<ExtentTest> stepTestThreadLocal = new InheritableThreadLocal<>();
  private static boolean scenarioOutlineFlag;
  private static File reportDirectory;
  private ThreadLocal<String> reportFilePath = new ThreadLocal<>();
  private ThreadLocal<File> file = new ThreadLocal<>();
  private String slash = File.separator;

  public ReportListener() {

    reportFilePath.set(
        System.getProperty("user.dir") + slash + "output" + slash + "Automation_Report_" + System
            .currentTimeMillis() + ".html");

    file.set(new File(reportFilePath.get()));

    setExtentHtmlReport(file.get());
    setExtentReport();
    stepListThreadLocal.set(new LinkedList<Step>());
    scenarioOutlineFlag = false;
  }

  public synchronized static void initiateCucumberReport(File filePath, Boolean replaceExisting) {
    reportDirectory = filePath;
    setExtentHtmlReport(filePath);
    setExtentReport();
    stepListThreadLocal.set(new LinkedList<Step>());
    scenarioOutlineFlag = false;
  }

  public synchronized void initiateCucumberReport(File filePath) {
    initiateCucumberReport(filePath, false);
  }

  private synchronized static void setExtentHtmlReport(File file) {
    if (htmlReporter.get() != null) {
      return;
    }
    if (!file.exists()) {
      file.getParentFile().mkdirs();
    }
    htmlReporter.set(new ExtentHtmlReporter(file));

    // Report configurations
    htmlReporter.get().config()
        .setJS(
            "$(document).ready(function() { var link = document.createElement('link');link.type = 'image/x-icon';link.rel = 'shortcut icon';link.href = 'https://www.seleniumhq.org/selenium-favicon.ico';document.getElementsByTagName('head')[0].appendChild(link);  });"
                + "$(document).ready(function() { var a = document.getElementsByClassName('brand-logo')[0]; a.innerHTML=''; a.href = '';});");
    htmlReporter.get().config().setDocumentTitle("QA Automation Report");
    htmlReporter.get().config().setReportName("QA Automation Report");
    htmlReporter.get().setAppendExisting(true);

  }

  static synchronized ExtentHtmlReporter getExtentHtmlReport() {
    return htmlReporter.get();
  }

  private synchronized static void setExtentReport() {
    if (extentReports.get() != null) {
      return;
    }
    extentReports.set(new ExtentReports());
    extentReports.get().attachReporter(htmlReporter.get());
  }

  static synchronized ExtentReports getExtentReport() {
    return extentReports.get();
  }

  public void syntaxError(String state, String event, List<String> legalEvents, String uri,
      Integer line) {

  }

  public void uri(String uri) {

  }

  public synchronized void feature(Feature feature) {
    featureTestThreadLocal.set(getExtentReport().createTest("Feature: " + feature.getName()));
    ExtentTest test = featureTestThreadLocal.get();

    for (Tag tag : feature.getTags()) {
      test.assignCategory(tag.getName());
    }
  }

  public void scenarioOutline(ScenarioOutline scenarioOutline) {
    scenarioOutlineFlag = true;
  }

  public void examples(Examples examples) {
  }

  public void startOfScenarioLifeCycle(Scenario scenario) {

    scenarioOutlineThreadLocal
        .set(featureTestThreadLocal.get().createNode("Scenario: " + scenario.getName()));

    for (Tag tag : scenario.getTags()) {
      scenarioOutlineThreadLocal.get().assignCategory(tag.getName());
    }

    if (scenarioOutlineFlag) {
      scenarioOutlineFlag = false;
    }
  }

  public void background(Background background) {

  }

  public void scenario(Scenario scenario) {

  }

  public void step(Step step) {
    if (scenarioOutlineFlag) {
      return;
    }
    stepListThreadLocal.get().add(step);
  }

  public void endOfScenarioLifeCycle(Scenario scenario) {

  }

  public void done() {
    getExtentReport().flush();
  }

  public void close() {

  }

  public void eof() {

  }

  public void before(Match match, Result result) {

  }

  public void result(Result result) {
    if (scenarioOutlineFlag) {
      return;
    }

    Step step = stepListThreadLocal.get().poll();

    if (Result.PASSED.equals(result.getStatus())) {
      scenarioOutlineThreadLocal.get().pass(step.getKeyword() + step.getName());
    } else if (Result.FAILED.equals(result.getStatus())) {
      scenarioOutlineThreadLocal.get()
          .fail(step.getKeyword() + step.getName() + "\\n" + result.getError());
    } else if (Result.SKIPPED.equals(result)) {
      scenarioOutlineThreadLocal.get().skip(step.getKeyword() + step.getName());
    } else if (Result.UNDEFINED.equals(result)) {
      scenarioOutlineThreadLocal.get().skip(step.getKeyword() + step.getName());
    }
  }

  public void after(Match match, Result result) {

  }

  public void match(Match match) {
  }

  public void embedding(String mimeType, byte[] data) {

  }

  public void write(String text) {

  }

  /**
   * Adds the screenshot from the given path to the current step
   *
   * @param imagePath The image path
   * @throws IOException Exception if imagePath is erroneous
   */
  public static void addScreenCaptureFromPath(String imagePath) throws IOException {
    getCurrentStep().addScreenCaptureFromPath(imagePath);
  }

  private static ExtentTest getCurrentStep() {
    return scenarioOutlineThreadLocal.get();
  }

}
