package com.ntrs.ksd.fundservice.automation.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
	FileInputStream file = null;
	XSSFWorkbook workbook = null;
	XSSFSheet sheet = null;

	public ExcelReader() {
	}

	public ExcelReader(String testDataFilePath) {
		try {
			file = new FileInputStream(testDataFilePath);
		} catch (FileNotFoundException e) {
		}
	}

	public ExcelReader(String testDataFilePath, String sheetName) {
		try {
			file = new FileInputStream(testDataFilePath);
			workbook = new XSSFWorkbook(file);
			sheet = workbook.getSheet(sheetName);
		} catch (Exception e) {
		}
	}

	/**
	 * Writes Excel at the said row,col
	 * 
	 * @param testDataFilePath
	 * @param sheetName
	 * @param Col
	 * @param Row
	 * @param result
	 */
	public void writeExcel(String testDataFilePath, String sheetName, int Col, int Row, String result) {
		try {
			file = new FileInputStream(testDataFilePath);
			workbook = new XSSFWorkbook(file);
			sheet = workbook.getSheet(sheetName);
			XSSFRow sheetrow = sheet.getRow(Row);

			DataFormat format = workbook.createDataFormat();
			CellStyle style = workbook.createCellStyle();
			style.setDataFormat(format.getFormat("text"));
			Cell cell = sheetrow.getCell(Col);
			if (cell == null)
				cell = sheetrow.createCell(Col);
			cell.setCellStyle(style);
			cell.setCellValue(result);
			file.close();

			FileOutputStream outFile = new FileOutputStream(new File(testDataFilePath));
			workbook.write(outFile);
			workbook.close();
			outFile.close();
		} catch (Exception e) {
		}
	}

	/**
	 * Reads said Excel sheet data and returns array of Strings
	 * 
	 * @param testDataFilePath
	 * @param sheetName
	 * @return
	 * @throws IOException
	 */
	public String[][] readExcel(String testDataFilePath, String sheetName) throws IOException {
		String[][] requestData = null;
		InputStream file = new FileInputStream(testDataFilePath);
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheet(sheetName);
		requestData = new String[sheet.getPhysicalNumberOfRows()][sheet.getRow(0).getLastCellNum()];
		for (int r = 0; r < sheet.getPhysicalNumberOfRows(); r++) {
			Row row = sheet.getRow(r);
			for (int c = 0; c < sheet.getRow(0).getLastCellNum(); c++) {
				Cell cell = row.getCell(c);
				requestData[r][c] = new DataFormatter().formatCellValue(cell);
			}
		}
		workbook.close();
		file.close();

		return requestData;
	}

	public  List<Hashtable<String, String>> readTestDataFromExcel(String sWorkBookName, String sWorkbookSheetName, String tableName) {
		String testDataFolderPath = System.getProperty("user.dir") + "\\testdata";
		// System.out.println(new File(testDataFolderPath).listFiles().length);
		List<Hashtable<String, String>> testData = null;
		for (File testDataFile : new File(testDataFolderPath).listFiles()) {
			// System.out.println("testDataFile.getAbsolutePath():::"+testDataFile.getAbsolutePath());
			if (!testDataFile.getAbsolutePath().contains("$")) {
				FileInputStream file = null;
				if (testDataFile.getPath().contains(sWorkBookName)) {
					try {
						file = new FileInputStream(testDataFile);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
					Workbook workBook = null;
					try {
						workBook = WorkbookFactory.create(file);
					} catch (Exception e) {
						e.printStackTrace();
					}
					Sheet testDataSheet = workBook.getSheet(sWorkbookSheetName);
					testData = getTestDataBySheet(testDataSheet, tableName);
					// testData = getTestData(testDataSheet,tableName,workBook);
				}
			}
		}
		return testData;
	}

	public Iterator<Object[]> getIterator(List<Hashtable<String, String>> testDataList) {
		List<Object[]> iteratorList = new ArrayList<Object[]>();
		for (Map map : testDataList) {
			iteratorList.add(new Object[] { map });
		}
		return iteratorList.iterator();
	}

	public List<Hashtable<String, String>> getTestDataBySheet(Sheet testDataSheet, String tableName) {
		List<Hashtable<String, String>> testData = new ArrayList<Hashtable<String, String>>();
		int startRow, endRow;
		// System.out.println("testDataSheet"+testDataSheet);
		startRow = getStartRow(testDataSheet, tableName);
		List<String> headers = getHeaders(testDataSheet, startRow + 1);
		endRow = getEndRow(testDataSheet, tableName, startRow);
		for (int i = startRow + 2; i < endRow; i++) {
			Hashtable<String, String> map = new Hashtable<String, String>();
			Row dataRow = testDataSheet.getRow(i);
			Cell dataCell;
			for (int j = 1; j < headers.size(); j++) {
				if (!headers.get(j).equals("CELL NOT FOUND") && !headers.get(j).equals("MISSING CONTENT")) {
					try {
						dataCell = dataRow.getCell(j, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
						/*if (dataCell != null) {
							dataCell.setCellType(Cell.CELL_TYPE_STRING);
						}
						if (dataCell != null && !("".equals(dataCell.getStringCellValue().trim()))) {
							map.put(headers.get(j), dataCell.getStringCellValue());
						}*/
						dataCell.setCellType(Cell.CELL_TYPE_STRING);
						map.put(headers.get(j), dataCell.getStringCellValue());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			testData.add(map);

		}
		return testData;
	}

	public int getStartRow(Sheet testDataSheet, String tableName) {
		for (int i = 0; i < testDataSheet.getLastRowNum() + 1; i++) {
			try {
				if (testDataSheet.getRow(i).getCell(0).getStringCellValue().toString().equalsIgnoreCase(tableName))
					return i;
			} catch (Exception e) {
			}
		}
		return 0;
	}

	private int getEndRow(Sheet testDataSheet, String tableName, int startRow) {
		for (int i = startRow + 1; i < testDataSheet.getLastRowNum() + 1; i++) {
			try {
				if (testDataSheet.getRow(i).getCell(0).getStringCellValue().toString().equalsIgnoreCase(tableName))
					return i;
			} catch (Exception e) {

			}
		}
		return startRow;
	}

	public List<String> getHeaders(Sheet sheet, int row) {

		List<String> headers = new ArrayList<String>();
		Row headerRow = sheet.getRow(row);
		for (int i = 0; i < headerRow.getLastCellNum(); i++) {
			Cell dataCell = headerRow.getCell(i, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
			if (dataCell == null)
				headers.add("CELL NOT FOUND");
			else if ("".equals(dataCell.getStringCellValue().trim()))
				headers.add("MISSING CONTENT");
			else {
				dataCell.setCellType(Cell.CELL_TYPE_STRING);
				headers.add(dataCell.getStringCellValue());
			}
		}
		return headers;

	}
}
