package com.ntrs.ksd.fundservice.automation.stepdefinitions.ui;

import com.ntrs.ksd.fundservice.automation.utils.Driver;
import com.ntrs.ksd.fundservice.automation.utils.ReportListener;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import java.io.File;
import java.util.UUID;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class CommonUISteps extends Driver {

  @Before
  public void beforeScenario() {
    startBrowser();
  }

  @After
  public void afterScenario() throws Exception {
    String fileSeparator = File.separator;
    String reportFilePath =
        System.getProperty("user.dir") + fileSeparator + "output" + fileSeparator
            + "Screenshots" + fileSeparator + UUID.randomUUID().toString() + ".jpeg";
    // Take screenshot and store as a file format
    File src = ((TakesScreenshot) driver.get()).getScreenshotAs(OutputType.FILE);
    // now copy the screenshot to desired location using copyFile //method
    FileUtils.copyFile(src, new File(reportFilePath));
    ReportListener.addScreenCaptureFromPath(reportFilePath);
    closeBrowser();
  }
}
