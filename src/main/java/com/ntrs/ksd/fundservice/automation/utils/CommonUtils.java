package com.ntrs.ksd.fundservice.automation.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

public class CommonUtils {
	public static String covertDate2DBDate(long longDate) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		return (df.format(new Date(longDate)));
	}

	public static String getRandomString(int size) {
		return "AUTO-"
				+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss:SSS_dd-MM-yyyy")).substring(0, size);
	}
}
