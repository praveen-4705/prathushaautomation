package com.ntrs.ksd.fundservice.automation.stepdefinitions.api;

import com.ntrs.ksd.fundservice.automation.utils.BaseTest;
import com.ntrs.ksd.fundservice.automation.utils.ExcelReader;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.junit.Assume;

import java.util.HashMap;
import java.util.Map;

public class CommonStepDefinitions extends BaseTest {

	@Before
	public void setup(Scenario scn) {
		if (!(featureName.isEmpty()) && !(scn.getId().split(";")[0].equalsIgnoreCase(featureName))) {
			isNewFeature = true;
		}
		if (isSkipped)
			Assume.assumeFalse(true);
	}

	@After
	public void tearDown(Scenario scn) {
		featureName = scn.getId().split(";")[0];
		if (!isNewFeature && (scn.isFailed() || scn.getStatus().equalsIgnoreCase("Skipped")))
			isSkipped = true;
	}

	@Given("^I start API test \"([^\"]*)\"$")
	public void i_start_API_test(String name) {
		testScriptName = name;
	}

	@Given("^I fetch the data from \"([^\"]*)\"$")
	public void i_fetch_the_data_from(String excelFile) {
		testCaseTestData = new ExcelReader().readTestDataFromExcel(excelFile, "Sheet1", testScriptName);
	}

	@And("^I add header information to request$")
	public void i_add_header_information_to_request() {
		Map<String, String> headerMap = new HashMap<String, String>();
		String headders = testCaseTestData.get(0).get("HEADERS");
		for (String item : headders.split(";")) {
			String[] itemArr = item.split("=");
			headerMap.put(itemArr[0], itemArr[1]);
		}
		restAssuredGiven.headers(headerMap);
	}

	@Then("^I validate status code in response$")
	public void i_validate_status_code_in_response() {
		response.then().assertThat().statusCode(Integer.parseInt(testCaseTestData.get(0).get("HEADERSStatus_Code")));
	}

	@Then("^I validate content type in response$")
	public void i_validate_content_type_in_response() {
		response.then().assertThat().contentType(testCaseTestData.get(0).get("CONTENT_TYPE"));
	}

	@Then("^I validate specified field values in response$")
	public void i_validate_specified_field_values_in_response() {
		int err = 0;

		String verifications = testCaseTestData.get(0).get("VERIFICATIONS");
		for (String item : verifications.split(";")) {
			String[] itemArr = item.split("=");
			try {
				String tagValue = globalMap.get(itemArr[0]).toString();
				if (!tagValue.isEmpty()) {
					Assert.assertTrue("", itemArr[1].contentEquals(tagValue));
				}
			} catch (AssertionError ae) {
				err++;
			}
			if (err > 0)
				Assert.fail("Tag value(s) do not match. Refer Test data for details.");
		}
	}
}
