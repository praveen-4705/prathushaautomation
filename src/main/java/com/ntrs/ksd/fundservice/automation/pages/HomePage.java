package com.ntrs.ksd.fundservice.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {

    @FindBy(id = "lst-ib")
    private WebElement tbSearch;

    @FindBy(name = "btnK")
    private WebElement btnSearch;

    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void searchWith(String keyWord) {
        clearAndEnter(tbSearch, keyWord);
        btnSearch.click();
        waitFor(3);
    }
}
