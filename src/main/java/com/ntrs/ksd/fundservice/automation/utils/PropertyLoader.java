package com.ntrs.ksd.fundservice.automation.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyLoader {
	private static PropertyLoader instance = null;
	private Properties properties;

	protected PropertyLoader() {
		try {
			String env = getEnv();
			properties = new Properties();
			// properties.load(getClass().getResourceAsStream("testing.properties"));
			String propFileName = System.getProperty("user.dir") + "/env/" + env + ".properties";
			properties.load(new FileInputStream(new File(propFileName)));
		} catch (Exception ex) {
		}
	}

	public String getEnv() {
		String env = "";
		try {
			String propFileName = System.getProperty("user.dir") + "/env/master.properties";
			Properties prop = new Properties();

			prop.load(new FileInputStream(new File(propFileName)));
			env = prop.getProperty("env");
		} catch (Exception ex) {
		}
		return env;
	}

	public static PropertyLoader getInstance() {
		if (instance == null) {
			instance = new PropertyLoader();
		}
		return instance;
	}

	public String getValue(String key) {
		return properties.getProperty(key);
	}
}
